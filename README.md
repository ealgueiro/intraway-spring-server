# Intraway Users REST API Server
Hi, my name is Esteban Algueiro and I developed this server as a technical challenge for applying for a position at Intraway.

In the following sections I will explain how to use the server and how the development process went through. 


## Starting the Server
You can start the server right away by running its JAR file in the target folder:

cd target

java -jar swagger-spring-1.0.0.jar


You can access a nice UI that has all the requests ready to be tested by pointing you browser to:

http://localhost:8080/




## Development Process  
I was given just a Swagger YAML file describing the different requests that needed to be included in a REST API. So, I just did some research regarding the Swagger tool and generated a Java SpringBoot Server after tweaking the provided YAML file a little bit.

I added database support as I needed it to perform the CRUD operations for the Users. I added a HyperSQL database because it works in-memory and it suffices this project. I also configured the User entity as a JPA Entity.

The application's main class is Swagger2SpringBoot.java, a SpringBoot application with a Tomcat embedded server. It is configured in file src/main/resources/application.properties.

This application uses Maven to build and test the project. You can see all the dependencies used by opening the pom.xml file at the root level. And you can build the JAR file with the following command:

mvn clean install

The REST API interface is described in file src/main/java/io/swagger/api/UsersApi.java, and its implementation is in src/main/java/io/swagger/api/UsersApiController.java.

The User entity is src/main/java/io/swagger/model/User.java and the JPA repository used for CRUD operations is src/main/java/io/swagger/persistence/UserRepository.java.

I provide a minimum test coverage for the REST API in file src/test/java/io/swagger/api/UsersApiTest.java. It can be run for example with the following command:

mvn test



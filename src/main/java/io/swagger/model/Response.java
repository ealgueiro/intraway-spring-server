package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Response
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-12-07T21:33:43.083Z")

public class Response {
	@JsonProperty("uuid")
	private UUID uuid = null;

	@JsonProperty("message")
	private String message = null;

	public Response uuid(UUID uuid) {
		this.uuid = uuid;
		return this;
	}

	/**
	 * Universally Unique TX Identifier
	 * 
	 * @return uuid
	 **/
	@ApiModelProperty(example = "123e4567-e89b-12d3-a456-426655440000", value = "Universally Unique TX Identifier")

	@Valid

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public Response message(String message) {
		this.message = message;
		return this;
	}

	/**
	 * Short Result Description
	 * 
	 * @return message
	 **/
	@ApiModelProperty(value = "Short Result Description")

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Response response = (Response) o;
		return Objects.equals(this.uuid, response.uuid) && Objects.equals(this.message, response.message);
	}

	@Override
	public int hashCode() {
		return Objects.hash(uuid, message);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Response {\n");

		sb.append("    uuid: ").append(toIndentedString(uuid)).append("\n");
		sb.append("    message: ").append(toIndentedString(message)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * User
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-12-07T21:33:43.083Z")
@Entity
public class User {
	
	@JsonProperty("username")
	private String username = null;

	@JsonProperty("firstName")
	private String firstName = null;

	@JsonProperty("lastName")
	private String lastName = null;

	@JsonProperty("email")
	private String email = null;

	@JsonProperty("password")
	private String password = null;

	@JsonProperty("phone")
	private String phone = null;

	/**
	 * User Status
	 */
	public enum UserStatusEnum {
		ACTIVE("Active"),

		BLOCKED("Blocked");

		private String value;

		UserStatusEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(value);
		}

		@JsonCreator
		public static UserStatusEnum fromValue(String text) {
			for (UserStatusEnum b : UserStatusEnum.values()) {
				if (String.valueOf(b.value).equals(text)) {
					return b;
				}
			}
			return null;
		}
	}

	@JsonProperty("userStatus")
	private UserStatusEnum userStatus = null;

	public User username(String username) {
		this.username = username;
		return this;
	}


	/**
	 * User Nick
	 * 
	 * @return username
	 **/
	@ApiModelProperty(example = "nacho", value = "User Nick")
	@Id
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public User firstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	/**
	 * User First Name
	 * 
	 * @return firstName
	 **/
	@ApiModelProperty(example = "Ignacio", value = "User First Name")

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public User lastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	/**
	 * User Last Name
	 * 
	 * @return lastName
	 **/
	@ApiModelProperty(example = "Galieri", value = "User Last Name")

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public User email(String email) {
		this.email = email;
		return this;
	}

	/**
	 * User e-mail
	 * 
	 * @return email
	 **/
	@ApiModelProperty(value = "User e-mail")

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public User password(String password) {
		this.password = password;
		return this;
	}

	/**
	 * User Password
	 * 
	 * @return password
	 **/
	@ApiModelProperty(example = "Nub1C4llSup3RS3GuR0", value = "User Password")

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User phone(String phone) {
		this.phone = phone;
		return this;
	}

	/**
	 * User Phone Number
	 * 
	 * @return phone
	 **/
	@ApiModelProperty(example = "555-5555", value = "User Phone Number")

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public User userStatus(UserStatusEnum userStatus) {
		this.userStatus = userStatus;
		return this;
	}

	/**
	 * User Status
	 * 
	 * @return userStatus
	 **/
	@ApiModelProperty(value = "User Status")

	public UserStatusEnum getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatusEnum userStatus) {
		this.userStatus = userStatus;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		User user = (User) o;
		return 
				Objects.equals(this.username, user.username) && Objects.equals(this.firstName, user.firstName)
				&& Objects.equals(this.lastName, user.lastName) && Objects.equals(this.email, user.email)
				&& Objects.equals(this.password, user.password) && Objects.equals(this.phone, user.phone)
				&& Objects.equals(this.userStatus, user.userStatus);
	}

	@Override
	public int hashCode() {
		return Objects.hash(username, firstName, lastName, email, password, phone, userStatus);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class User {\n");

		sb.append("    username: ").append(toIndentedString(username)).append("\n");
		sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
		sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
		sb.append("    email: ").append(toIndentedString(email)).append("\n");
		sb.append("    password: ").append(toIndentedString(password)).append("\n");
		sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
		sb.append("    userStatus: ").append(toIndentedString(userStatus)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

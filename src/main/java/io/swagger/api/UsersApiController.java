package io.swagger.api;

import io.swagger.model.Response;
import io.swagger.model.User;
import io.swagger.persistence.UserRepository;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-12-07T21:33:43.083Z")

@Controller
public class UsersApiController implements UsersApi {

    private static final Logger log = LoggerFactory.getLogger(UsersApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    public UsersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Response> createUser(@ApiParam(value = "Created user object"  )  @Valid @RequestBody User body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                userRepository.save(body);
                userRepository.flush();
            	return new ResponseEntity<Response>(objectMapper.readValue("{  \"message\" : \"message\",  \"uuid\" : \"123e4567-e89b-12d3-a456-426655440000\"}", Response.class), HttpStatus.CREATED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Response>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        if (accept != null && accept.contains("application/xml")) {
            try {
                return new ResponseEntity<Response>(objectMapper.readValue("<null>  <uuid>123e4567-e89b-12d3-a456-426655440000</uuid>  <message>aeiou</message></null>", Response.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/xml", e);
                return new ResponseEntity<Response>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Response>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Response> deleteUser(@ApiParam(value = "The name that needs to be deleted.",required=true) @PathVariable("username") String username) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                User user = userRepository.findOne(username);
                
                if (user == null){
                	return new ResponseEntity<Response>(objectMapper.readValue("{  \"message\" : \"message\",  \"uuid\" : \"123e4567-e89b-12d3-a456-426655440000\"}", Response.class), HttpStatus.NOT_FOUND); 
                }

            	userRepository.delete(username);
                userRepository.flush();
            	return new ResponseEntity<Response>(objectMapper.readValue("{  \"message\" : \"message\",  \"uuid\" : \"123e4567-e89b-12d3-a456-426655440000\"}", Response.class), HttpStatus.OK);

            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Response>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Response>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<User> findUserByUsername(@ApiParam(value = "The name that needs to be fetched.",required=true) @PathVariable("username") String username) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                User user = userRepository.findOne(username);
                
                if (user == null){
                	return new ResponseEntity<User>(user, HttpStatus.NOT_FOUND); 
                }

                return new ResponseEntity<User>(user, HttpStatus.OK);
                
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<User>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Response> modifyUser(@ApiParam(value = "The name that needs to be modified.",required=true) @PathVariable("username") String username,@ApiParam(value = "User object to update"  )  @Valid @RequestBody User body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                User user = userRepository.findOne(username);
                
                if (user == null){
                	return new ResponseEntity<Response>(objectMapper.readValue("{  \"message\" : \"message\",  \"uuid\" : \"123e4567-e89b-12d3-a456-426655440000\"}", Response.class), HttpStatus.NOT_FOUND);
                }

                //Merges body with editable fields in user
                user.setEmail(body.getEmail() != null? body.getEmail() : user.getEmail());
                user.setFirstName(body.getFirstName() != null? body.getFirstName() : user.getFirstName());
                user.setLastName(body.getLastName() != null? body.getLastName() : user.getLastName());
                user.setPassword(body.getPassword() != null? body.getPassword() : user.getPassword());
                user.setPhone(body.getPhone() != null? body.getPhone() : user.getPhone());
                
                userRepository.save(user);
                userRepository.flush();
                
            	return new ResponseEntity<Response>(objectMapper.readValue("{  \"message\" : \"message\",  \"uuid\" : \"123e4567-e89b-12d3-a456-426655440000\"}", Response.class), HttpStatus.OK);
            	
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Response>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Response>(HttpStatus.NOT_IMPLEMENTED);
    }

}

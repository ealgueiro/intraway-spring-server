package io.swagger.api;

import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import io.swagger.Swagger2SpringBoot;

/**
 * API tests for UsersApi
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class UsersApiTest {
	
	Swagger2SpringBoot server;
	
    public UsersApiTest() throws Exception {
   		server = new Swagger2SpringBoot();
   		server.main(new String[0]);
	}
  
    /**
     * 
     * @throws ClientProtocolException
     * @throws IOException
     */
    @Test
    public void usersApiTest() throws ClientProtocolException, IOException {
        /**
         *  Create User
         */
    	//Given
    	HttpPost post = new HttpPost( "http://localhost:8080/users");
    	post.addHeader("Accept", "application/json");
    	post.addHeader("Content-Type", "application/json");
    	post.setEntity(new StringEntity(
        				"{\"email\": \"string\","
        				+ " \"firstName\": \"Ignacio\","
        				+ " \"lastName\": \"Galieri\","
        				+ "\"password\": \"Nub1C4llSup3RS3GuR0\","
        				+ " \"phone\": \"555-5555\","
        				+ " \"userStatus\": \"Active\","
        				+ " \"username\": \"nacho\"}"
        				));
      
        // When
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute( post );
      
        // Then
        assert(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED);

        
        /**
         * Get User
         */
        // Given
        HttpGet get = new HttpGet( "http://localhost:8080/users/nacho");
        get.addHeader("Accept", "application/json");
        get.addHeader("Content-Type", "application/json");
      
        // When
        httpResponse = HttpClientBuilder.create().build().execute( get );
      
        // Then
        assert(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK);


    	/**
    	 * Get non-existent user
    	 */
       // Given
       String name = "blabla";
       get = new HttpGet( "http://localhost:8080/users/" + name );
       get.addHeader("Accept", "application/json");
     
       // When
       httpResponse = HttpClientBuilder.create().build().execute( get );
     
       // Then
       assert(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_FOUND);
    
    
       /**
	    * Modify Complete User
	    */
        // Given
    	HttpPut put = new HttpPut( "http://localhost:8080/users/nacho");
    	put.addHeader("Accept", "application/json");
    	put.addHeader("Content-Type", "application/json");
    	put.setEntity(new StringEntity(
        				"{\"email\": \"otroemail\"}"
        				));
      
        // When
        httpResponse = HttpClientBuilder.create().build().execute( put );
      
        // Then
        assert(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK);

        
	    /**
	     * Delete User
	     */
        // Given
    	HttpDelete delete = new HttpDelete("http://localhost:8080/users/nacho");
    	delete.addHeader("Accept", "application/json");
    	delete.addHeader("Content-Type", "application/json");
      
        // When
        httpResponse = HttpClientBuilder.create().build().execute( delete );
      
        // Then
        assert(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK);
    }
    
}
